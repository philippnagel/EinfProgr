#My solutions for Einführung in die Programmierung

**In case you are taking the same course please do not get too inspired by my solutions. They might be bad and you won't learn anything. Getting caught sucks as well.**

These are my solutions for the course Einführung in die Programmierung offered during the winter semester 2015/2015 at LMU Munich. Please excuse the mixture of English and German since the course is being held in German.

The course website can be found here: [https://www.tcs.ifi.lmu.de/lehre/ws-2015-16/eip/](https://www.tcs.ifi.lmu.de/lehre/ws-2015-16/eip/).
