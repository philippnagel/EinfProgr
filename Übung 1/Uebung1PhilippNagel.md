#Übung 1
##Philipp Nagel

###A1-1

Das Ausführen von ``javac Anfang.java`` bewirkt, dass der Java Compiler aus unserem Java Code Bytecode erzeugt.

``java Anfang`` führt den so erzeugten Bytecode dann aus. In diesem Fall wird dadurch "Aller Anfang ist einfach ausgegeben".

###A1-2

1. Die Klasse hat einen anderen Namen als die Datei.
2. Void fehlt in diesem Programm, da dieses keine Prozeduren verwendet.
3. In Zeile 3 fehlt bei ``public static main(String args)`` das ``[]`` bei ``String`` 
4. Zeile 6 muss von geschwungenen Klammern ``{}`` umgeben sein.
5. Die Funktion ``system.out.print(Hello World!)`` beinhaltet drei Fehler. ``System`` muss groß geschrieben werden, da es eine Methode ist. Es fehlen die Anführungszeichen bei ``("Hello World!")``. Außerdem muss mit einem ``;`` signalisiert werden, dass die Funktion abgeschlossen ist.

Der korrigierte Code sieht dann folgendermaßen aus:

```
public class Fehlerprogramm
{
  public static void main(String[] args){

  System.out.print("Hello World!");

  }
}
```